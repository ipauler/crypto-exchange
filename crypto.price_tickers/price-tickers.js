const a8 = require('@autom8/js-a8-fdk')
// const a8 = require('js-a8-link')('http://localhost:3035')

const chart = async (params) => {

  let data = await a8.ccxt.ohlcv({
    ...params,
    from: new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24) / 1000,
    to: new Date() / 1000,
    interval: '5m'
  })
  
  let ohlcv =
    data.ohlcv.map(e => {
      return { ...e, symbol: params.exchange + ' ' + params.currency }
    })

  let colors

  if (params.theme == 'light')
    colors = {
      background: '#292f3b',
      xAxis: 'darkgrey',
      color1: 'orange',
      color2: 'steelblue',
    }
  else
    colors = {
      background: '#ffffff',
      xAxis: '#292f3b',
      color1: 'orange',
      color2: 'steelblue',
    }

  const chart = await a8.graphs.multiline_chart({ ohlcv, colors })

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.data
}

module.exports = { chart }