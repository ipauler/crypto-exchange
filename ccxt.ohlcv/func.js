const fdk = require('@autom8/fdk')
const a8 = require('@autom8/js-a8-fdk')
const ohlcv = require('./ohlcv')

const slackLimit = 25
const discordLimit = 15

const defaults = {
  exchange: 'bitfinex',
  currency: 'BTC/USD',
  interval: '1h',
  from: new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24) / 1000,
  to: new Date() / 1000
}

const handle = async (input) => {

  if (!input.currency)
    return { error: "Please enter currency" }

  const params = {
    currency: input.currency || defaults.currency,
    exchange: input.exchange || defaults.exchange,
    interval: input.interval || defaults.interval,
    from: input.from || defaults.from,
    to: input.to || defaults.to
  }

  try {
    const response = await ohlcv.price(params)
    return { ohlcv: response }
  } catch (err) {
    return { error: err.message }
  }
}

const slack = (result) => {
  let responseType = "in_channel"
  const blocks = []

  if (!result.error) {
    if (result.ohlcv.length > slackLimit)
      result.ohlcv = result.ohlcv.slice(result.ohlcv.length - slackLimit, result.ohlcv.length)

    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": " ``` " + JSON.stringify(result.ohlcv) + " ``` "
      }
    })


  } else {
    responseType = "ephemeral"
    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "Error retrieving ohlcv data."
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
}
const discord = (result) => {

  if (!result.error) {
    if (result.ohlcv.length > discordLimit)
      result.ohlcv = result.ohlcv.slice(result.ohlcv.length - discordLimit, result.dohlcvata.length)
    return {
      "content": "``` " + JSON.stringify(result.ohlcv) + " ```"
    }
  }
  else {
    return {
      "content": "Error generating chart."
    }
  }
}
// module.exports = { handle }
fdk.handle(handle)
fdk.slack(slack)
fdk.discord(discord)