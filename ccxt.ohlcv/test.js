const ohlcv = require('./ohlcv');
// const func = require('./func')

// ohlcv.price({ currency: 'BTC/USD', exchange: 'bitfinex', from: 1556709487, to: 1556882287, interval: '15m' })
ohlcv.price({
  currency: 'BTC/USD', exchange: 'bitfinex', interval: '15m',
  from: new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24) / 1000,
  to: new Date() / 1000,
  interval: '5m'
}).then(resp => { console.log(resp.length) })
  .catch(err => { console.log(err) });



// func.handle({ currency: 'BTC/USD' })
//   .then(resp => { console.log(resp) })
//   .catch(err => { console.log(err, 1) });
