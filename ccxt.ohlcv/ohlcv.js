const ccxt = require('ccxt')
const moment = require('moment')
moment.suppressDeprecationWarnings = true

const price = async (params) => {

  if (ccxt.exchanges.indexOf(params.exchange) === -1)
    throw new Error('Sorry this exchange market is not supported')

  let from, to

  try {

    from = (moment.unix(params.from).isValid() ? moment.unix(params.from) : moment(params.from)).valueOf()
    to = (moment.unix(params.to).isValid() ? moment.unix(params.to) : moment(params.to)).valueOf()
  } catch (err) {
    throw new Error('Wrong date time format')
  }

  if (!params.interval)
    throw new Error('Wrong date time format')

  const limit = (to - from) / timeframeToMs(params.interval)


  const ohlcv = await new ccxt[params.exchange]().fetchOHLCV(params.currency, params.interval, from, limit)

  const result = ohlcv.map((elem) => {
    let [date, open, high, low, close, value] = elem
    return {
      timestamp: date, open, high, low, close, value
    }
  })

  return result
}

const timeframeToMs = (timeframe) => {
  let ms = new Date().getTime()
  if (!timeframe)
    return ms

  const format = timeframe.substr(-1).toLowerCase()
  const value = timeframe.slice(0, -1)

  switch (format) {
    case 'm':
      ms = milliseconds(0, value, 0)
      break
    case 'h':
      ms = milliseconds(value, 0, 0)
      break
    case 'd':
      ms = milliseconds(value * 24, 0, 0)
      break
    case 'm':
      ms = milliseconds(value * 24 * 30, 0, 0)
      break
  }
  return ms
}

const milliseconds = (h, m, s) => ((h * 60 * 60 + m * 60 + s) * 1000)

module.exports = { price }