const fs = require('fs')
const d3 = require('d3')
const JSDOM = require('jsdom').JSDOM
const svg2img = require('svg2img')
const Chart = require('./chart/chart')
const path = require('path')
const inlineCss = require('inline-css')
const imgur = require('imgur')
const util = require('util')

require('jsdom-global')()
imgur.setClientId('947ba3967a16848')

const chartSelector = 'chart'
const defaultColors = {
  background: '#292f3b',
  xAxis: 'darkgrey',
  candleRaise: 'lightgrey',
  candleFall: 'turquoise',
  volumeBar: 'darkgrey'
};

const draw = async (ohlcv, colors = defaultColors) => {

  let css = fs.readFileSync(path.normalize(`${__dirname}/chart/chart.css`), 'utf8')
  Object.entries(colors).forEach(([key, val]) => {
    css = css.replace(new RegExp('\\$' + key, 'g'), colors[key])
  })

  const mainHtml = fs.readFileSync(path.normalize(`${__dirname}/chart/chart.html`), 'utf8')

  const window = (new JSDOM(mainHtml,
    {
      features: {
        FetchExternalResources: ['script', 'css'],
        QuerySelector: true
      },
      pretendToBeVisual: true,
      runScripts: "outside-only"
    })).window

  window.d3 = d3.select(window.document)
  window.Date = Date


  const data = ohlcv.map(e => { e.timestamp = new Date(e.timestamp); return e })
  const chart = new Chart(d3, data, window.d3.select(`#${chartSelector}`))
  chart.render()

  const svg = window.document.querySelector('svg')
  const s = document.createElement('style')
  s.setAttribute('type', 'text/css')
  s.innerHTML = `<![CDATA[\n${css}\n]]>`
  const defs = document.createElement('defs')
  defs.appendChild(s)
  svg.insertBefore(defs, svg.firstChild)

  const html = await inlineCss(window.d3.select('#chart').html(), { url: 'filePath' })

  const svgToImg = util.promisify(svg2img)
  try {
    const buffer = await svgToImg(html)
    const json = await imgur.uploadBase64(buffer.toString('base64'))
    return json.data.link
  } catch (err) {
    throw new Error(err)
  }

}

module.exports = { draw }