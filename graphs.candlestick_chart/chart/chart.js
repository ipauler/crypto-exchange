const interval = 'day'
const TFormat = { 'day': "%d %b '%y", "week": "%d %b '%y", "month": "%b '%y" }

const bClassName = 'bar'

const margin = {
  candle: { top: 20, right: 35, bottom: 40, left: 5 },
  bar: { top: 450, right: 30, bottom: 10, left: 5 }
}

const width = 840,
  cHeight = 400,
  bHeight = 120,
  fHeight = 515

class CandlestickChart {

  constructor(d3, data, selection) {
    this.d3 = d3
    this.data = data
    this.elem = selection[0][0]
  }

  render() {
    this.candleChart(this.elem)
    this.barChart(this.elem)
  }

  candleChart(elem) {

    const minimal = this.d3.min(this.data, d => d.low)
    const maximal = this.d3.max(this.data, d => d.high)

    const x = this.d3.scale.ordinal()
      .rangeBands([0, width])
    const y = this.d3.scale.linear()
      .rangeRound([cHeight, 0])

    const xAxis = this.d3.svg.axis()
      .scale(x)
      .tickFormat(this.d3.time.format(TFormat[interval]))

    const yAxis = this.d3.svg.axis()
      .scale(y)
      .ticks(Math.floor(cHeight / 50))

    x.domain(this.data.map(d => d.timestamp))
    y.domain([minimal, maximal]).nice()

    const xtickdelta = Math.ceil(60 / (width / this.data.length))
    xAxis.tickValues(x.domain().filter((d, i) => !((i + Math.floor(xtickdelta / 2)) % xtickdelta)))

    const barwidth = x.rangeBand()
    const candlewidth = Math.floor(this.d3.min([barwidth * 0.8, 13]) / 2) * 2 + 1
    const delta = Math.round((barwidth - candlewidth) / 2)

    this.d3.select(elem).select('svg').remove()

    const svg = this.d3.select(elem).append('svg')
      .attr('width', width + margin.candle.left + margin.candle.right)
      .attr('height', fHeight + margin.candle.top + margin.candle.bottom)
      .attr('class', 'mainSvg')
      .style('font-family', 'Lato')
      .append('g')
      .attr('transform', `translate( ${margin.candle.left}  ,  ${margin.candle.top} )`)

    svg.append('rect')
      .attr('width', '110%')
      .attr('height', '100%')
      .attr('y', '-' + margin.candle.top)
      .attr('class', 'bg-rect')

    svg.append('g')
      .attr('class', 'axis xaxis')
      .attr('transform', `translate(0, ${cHeight} )`)
      .call(xAxis.orient("bottom").outerTickSize(0))

    svg.append('g')
      .attr('class', 'axis yaxis')
      .attr('transform', `translate( ${width} ,0)`)
      .call(yAxis.orient("right").tickSize(0))

    svg.append('g')
      .attr('class', 'axis grid')
      .attr('transform', `translate( ${width} ,0)`)
      .call(yAxis.orient('left').tickFormat('').tickSize(width).outerTickSize(0))

    const bands = svg.selectAll('.bands')
      .data([this.data])
      .enter().append('g')
      .attr('class', 'bands')

    bands.selectAll('rect')
      .data(d => d)
      .enter().append('rect')
      .attr('x', d => x(d.timestamp) + Math.floor(barwidth / 2))
      .attr('y', 0)
      .attr('height', fHeight)
      .attr('width', 1)
      // .attr('class', i => 'band' + i)
      .style('stroke-width', Math.floor(barwidth))

    const stick = svg.selectAll('.sticks')
      .data([this.data])
      .enter().append('g')
      .attr('class', 'sticks')

    stick.selectAll('rect')
      .data(d => d)
      .enter().append('rect')
      .attr('x', d => x(d.timestamp) + Math.floor(barwidth / 2))
      .attr('y', d => y(d.high))
      // .attr('class', i => 'stick' + i)
      .attr('height', d => y(d.low) - y(d.high))
      .attr('width', 1)
      .classed('rise', d => (d.close > d.open))
      .classed('fall', d => (d.open > d.close))

    const candle = svg.selectAll('.candles')
      .data([this.data])
      .enter().append('g')
      .attr('class', 'candles')

    candle.selectAll('rect')
      .data(d => d)
      .enter().append('rect')
      .attr('x', d => x(d.timestamp) + delta)
      .attr('y', d => y(this.d3.max([d.open, d.close])))
      // .attr('class', i => 'candle' + i)
      .attr('height', d => y(this.d3.min([d.open, d.close])) - y(this.d3.max([d.open, d.close])))
      .attr('width', candlewidth)
      .classed('rise', d => (d.close > d.open))
      .classed('fall', d => (d.open > d.close))

  }


  barChart(elem) {
    const x = this.d3.scale.ordinal()
      .rangeBands([0, width])

    const y = this.d3.scale.linear()
      .rangeRound([bHeight, 0])

    const xAxis = this.d3.svg.axis()
      .scale(x)
      .tickFormat(this.d3.time.format(TFormat[interval]))

    const yAxis = this.d3.svg.axis()
      .scale(y)
      .ticks(Math.floor(bHeight / 50))

    const svg = this.d3.select(elem).select('svg')
      .append('g')
      .attr('transform', `translate( ${margin.bar.left} , ${margin.bar.top} )`)

    x.domain(this.data.map(d => d.timestamp))
    y.domain([0, this.d3.max(this.data, d => d.value)]).nice()


    const xtickdelta = Math.ceil(60 / (width / this.data.length))
    xAxis.tickValues(x.domain().filter((d, i) => !((i + Math.floor(xtickdelta / 2)) % xtickdelta)))

    svg.append("g")
      .attr("class", "axis yaxis")
      .attr("transform", "translate(" + width + ",0)")
      .call(yAxis.orient("right").tickFormat("").tickSize(0))

    const barwidth = x.rangeBand()
    const fillwidth = (Math.floor(barwidth * 0.9) / 2) * 2 + 1
    const bardelta = Math.round((barwidth - fillwidth) / 2)

    const mbar = svg.selectAll("." + bClassName + "bar")
      .data([this.data])
      .enter().append("g")
      .attr("class", bClassName + "bar")
    // .attr("class",'volumebar')

    mbar.selectAll("rect")
      .data(d => d)
      .enter().append("rect")
      .attr("class", bClassName + "fill")
      // .attr("class", 'volumebar')
      .attr("x", d => x(d.timestamp) + bardelta)
      .attr("y", d => y(d.value))
      // .attr("style", "fill: darkgrey")
      .attr("class", (d, i) => bClassName + i)
      .attr("height", d => y(0) - y(d.value))
      .attr("width", fillwidth)

  }

}

module.exports = CandlestickChart