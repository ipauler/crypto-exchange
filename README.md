# CRYPTO FUNC

## #1 get ohlcv

`a8 invoke ccxt.ohlcv '{"currency":"BTC/USD", "exchange":"bitfinex","interval":"1d", "from":"2019-05-01 09:30:26", "to":"2019-05-01 09:30:26 "}'`

## #2 get ascii chart from ohlcv

`a8 invoke graphs.ascii_chart '{"ohlcv": [{"close": 7828.2, "high": 7845.8, "low": 7755,   "open": 7786.16438939, "timestamp": 1557774000000, "value": 923.17770403},{ "close": 7804.3, "high": 7856.3, "low": 7705, "open": 7826.8, "timestamp": 1557777600000, "value": 1234.20954766}]}'`

## #3 get price graph

`a8 invoke crypto.price_graph '{"currency":"BTC/USD"}'`

## #4 get candlestick chart from ohlcv

`a8 invoke graphs.candlestick_chart   '{"ohlcv": [{"close": 7828.2, "high": 7845.8, "low": 7755,   "open": 7786.16438939, "timestamp": 1557774000000, "value": 923.17770403},{ "close": 7804.3, "high": 7856.3, "low": 7705, "open": 7826.8, "timestamp": 1557777600000, "value": 1234.20954766}],"colors":{"background":"","xAxis":"","candleRaise":"","candleFall":"","volumeBar":""} }'`

## #5 get price chart

`a8 invoke crypto.price_chart  '{"currency":"BTC/USD","theme":"light", "exchange":"bitfinex","interval":"1d", "from":"2019-05-01 09:30:26", "to":"2019-05-01 09:30:26 "}'`

## #6 get multiline chart

`a8 invoke graphs.multiline_chart '{"ohlcv": [{"close": 5723.8,"high": 5784.3,"low": 5611.1,"open": 5629.6,"timestamp": 1556755200000,"value": 11075.8368328,"symbol": "bitfinex"},{"close": 6017,"high": 6150,"low": 5702.29694186,"open": 5736.6,"timestamp": 1556841600000,"value": 19480.28183518,"symbol": "bitfinex"},{"close": 5403.984,"high": 5419.188,"low": 5310,"open": 5325.815,"timestamp": 1556755200000,"value": 398.70670515,"symbol": "bittrex"},{"close": 5699.683,"high": 5794.2,"low": 5367.949,"open": 5390,"timestamp": 1556841600000,"value": 1116.63944662,"symbol": "bittrex"}]}'`

## #7 get arbitrage pairs chart

`a8 invoke crypto.arbitrage_pairs '{"exchanges":["bittrex","bitfinex"],"currency":"BTC/USD"}'`

## #8 get price tickers chart

`a8 invoke crypto.price_tickers '{"currency":"BTC/USD","exchange":"bittrex"}'`

## #9 get trading pairs chart

`a8 invoke crypto.trading_pairs '{"pairs":[ {"currency":"BTC/USD","exchange":"bittrex"} , {"currency":"BTC/USD","exchange":"bitfinex" }]}'`

## #10 get order book data

`a8 invoke ccxt.order_book '{"currency":"BTC/USD","exchange":"bitfinex"}'`