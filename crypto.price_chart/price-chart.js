const a8 = require('@autom8/js-a8-fdk')
// const a8 = require('js-a8-link')('http://localhost:3035')

const graph = async (params) => {

  const priceData = await a8.ccxt.ohlcv(params)

  if (priceData.error) {
    throw new Error(priceData.error)
  }
  let colors

  if (params.theme == 'light')
    colors = {
      background: '#292f3b',
      xAxis: 'darkgrey',
      candleRaise: 'lightgrey',
      candleFall: 'turquoise',
      volumeBar: 'darkgrey'
    }
  else
    colors = {
      background: '#ffffff',
      xAxis: '#292f3b',
      candleRaise: '#64b5f5',
      candleFall: '#ef6c00',
      volumeBar: '#8cc6f5'
    }

  const chart = await a8.graphs.candlestick_chart({ ohlcv: priceData.ohlcv, colors })

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.data
}

module.exports = { graph }