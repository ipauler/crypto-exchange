const fdk = require('@autom8/fdk')
const a8 = require('@autom8/js-a8-fdk')
const priceGraph = require('./price-chart')

const handle = (input) => {

  const params = {
    currency: input.currency,
    exchange: input.exchange,
    interval: input.interval,
    from: input.from,
    to: input.to,
    theme: input.theme
  }

  return priceGraph.graph(params)
    .then(response => {
      return { data: response }
    })
    .catch(err => {
      return { error: err.message }
    })
}

const slack = (result) => {
  let responseType = "in_channel";
  const blocks = [];

  if (!result.error) {
    blocks.push({
      "type": "image",
      "title": {
        "type": "plain_text",
        "text": "Candlestick chart",
        "emoji": true
      },
      "image_url": result.data,
      "alt_text": "chart"
    })
  } else {
    responseType = "ephemeral"
    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "Error generating chart. " + result.error
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
}

const discord = (result) => {

  if (!result.error) {
    if (result.data.length > discordLimit)
      result.data = result.data.slice(result.data.length - discordLimit, result.data.length);
    return {
      "content": "``` " + JSON.stringify(result.data) + " ```"
    }
  }
  else {
    return {
      "content": "Error generating chart."
    }
  }
}
// module.exports = {handle}
fdk.handle(handle);
fdk.slack(slack);
fdk.discord(discord);
