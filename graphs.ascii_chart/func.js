const fdk = require('@autom8/fdk');
const a8 = require('@autom8/js-a8-fdk');
const asciiChart = require('./ascii-chart');

const handle = (input) => {

	if (!input.ohlcv)
		return { error: "please provide some input data" }

	return asciiChart.chart(input.ohlcv, input.key)
		.then(response => {
			return { data: response }
		})
		.catch(error => {
			return { error }
		});
}

const slack = (result) => {
	let responseType = "in_channel";
	const blocks = [];

	if (!result.error) {
		blocks.push({
			"type": "image",
			"title": {
				"type": "plain_text",
				"text": "Current Bitcoin Price",
				"emoji": true
			},
			"image_url": result.chart,
			"alt_text": "bitcoinprice"
		})
	} else {
		responseType = "ephemeral"
		blocks.push({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": "Error generating chart."
			}
		})
	}

	return {
		"response_type": responseType,
		"blocks": blockst
	}
}
const discord = (result) => {

	if (!result.error) {
		return {
			"embed": {
				"image": {
					"url": link
				}
			}
		}
	}
	else {
		return {
			"content": "Error generating chart."
		}
	}
}

fdk.handle(handle);
fdk.slack(slack);
fdk.discord(discord);
