const imgur = require('imgur');
const asciichart = require('asciichart');
const text2png = require('text2png');

imgur.setClientId('947ba3967a16848')

const chart = async (ohlcv, key = 'close') => {

  const data = ohlcv.map(x => x[key]);

  const chart = asciichart.plot(data, { height: 30, offset: 2, padding: '' });
  const pngConfig = {
    font: '30px Source Code Pro',
    localFontPath: __dirname + '/SourceCodePro-Regular.ttf',
    localFontName: 'Source Code Pro',
    color: 'yellow',
    backgroundColor: 'black',
    output: 'buffer'
  };
  const dates = ohlcv.map(e => e.timestamp);

  const minDate = new Date(Math.min.apply(null, dates))
    .toISOString()
    .replace(/T/, ' ')
    .replace(/\..+/, '');

  const maxDate = new Date(Math.max.apply(null, dates))
    .toISOString()
    .replace(/T/, ' ')
    .replace(/\..+/, '');

  const description = `\n\n    Currency chart for ${minDate} - ${maxDate}      \n\n`;

  const buffer = await text2png(chart + description, pngConfig);

  return imgur.uploadBase64(buffer.toString('base64'))
    .then(function (json) {
      return json.data.link;
    })
    .catch(function (err) {
      console.error(err.message);
    });
}

module.exports = { chart };