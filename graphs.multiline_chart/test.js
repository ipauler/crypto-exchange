const chart = require('./multiline-chart')
// const func = require('./func')

const data = {
  bitfinex:
    [
      {
        "close": 5723.8,
        "high": 5784.3,
        "low": 5611.1,
        "open": 5629.6,
        "timestamp": 1556755200000,
        "value": 11075.8368328
      },
      {
        "close": 6017,
        "high": 6150,
        "low": 5702.29694186,
        "open": 5736.6,
        "timestamp": 1556841600000,
        "value": 19480.28183518
      },
      {
        "close": 6098.1,
        "high": 6189.9903581,
        "low": 5863.4,
        "open": 6019.9,
        "timestamp": 1556928000000,
        "value": 9437.42259102
      },
      {
        "close": 6041.4021233,
        "high": 6112.3,
        "low": 5956,
        "open": 6098.1649927,
        "timestamp": 1557014400000,
        "value": 5815.53348677
      },
      {
        "close": 6027.9,
        "high": 6102,
        "low": 5901,
        "open": 6041.4021233,
        "timestamp": 1557100800000,
        "value": 6477.39950895
      },
      {
        "close": 6100,
        "high": 6325,
        "low": 6028.9,
        "open": 6028.9,
        "timestamp": 1557187200000,
        "value": 11990.22369015
      },
      {
        "close": 6201.5,
        "high": 6230,
        "low": 6007.8,
        "open": 6103.8,
        "timestamp": 1557273600000,
        "value": 8222.18279599
      },
      {
        "close": 6262,
        "high": 6344,
        "low": 6153.4575144,
        "open": 6201.8,
        "timestamp": 1557360000000,
        "value": 9682.27718866
      },
      {
        "close": 6438.7,
        "high": 6522.2,
        "low": 6212.2,
        "open": 6262,
        "timestamp": 1557446400000,
        "value": 14493.48955268
      },
      {
        "close": 7125.1,
        "high": 7279,
        "low": 6438.7,
        "open": 6438.7,
        "timestamp": 1557532800000,
        "value": 37945.37174068
      },
      {
        "close": 6983.4,
        "high": 7448,
        "low": 6858,
        "open": 7125.1,
        "timestamp": 1557619200000,
        "value": 31958.82528419
      },
      {
        "close": 7770.7,
        "high": 8050,
        "low": 6879.9649408,
        "open": 6980.2,
        "timestamp": 1557705600000,
        "value": 36467.14132655
      },
      {
        "close": 7951.9,
        "high": 8194.1,
        "low": 7651,
        "open": 7770.7,
        "timestamp": 1557792000000,
        "value": 29342.89533348
      },
      {
        "close": 8136.6341756,
        "high": 8209,
        "low": 7832.45175911,
        "open": 7951.9,
        "timestamp": 1557878400000,
        "value": 19036.43866675
      },
      {
        "close": 7859.7,
        "high": 8352.3,
        "low": 7700.1,
        "open": 8132.5,
        "timestamp": 1557964800000,
        "value": 22313.8762668
      },
      {
        "close": 7417.9,
        "high": 7910,
        "low": 6999.9105737,
        "open": 7860.4,
        "timestamp": 1558051200000,
        "value": 29021.92287556
      },
      {
        "close": 7260.1,
        "high": 7518,
        "low": 7207.6,
        "open": 7421.7,
        "timestamp": 1558137600000,
        "value": 9319.33096968
      },
      {
        "close": 8188.3,
        "high": 8269.2,
        "low": 7249.34672701,
        "open": 7260,
        "timestamp": 1558224000000,
        "value": 23458.30829652
      },
      {
        "close": 7990.6,
        "high": 8188,
        "low": 7600.1,
        "open": 8185.10102918,
        "timestamp": 1558310400000,
        "value": 16901.81605768
      },
      {
        "close": 7931.71147491,
        "high": 8094,
        "low": 7814.8,
        "open": 7985.1,
        "timestamp": 1558396800000,
        "value": 7692.99194632
      },
      {
        "close": 7620,
        "high": 8039.9,
        "low": 7500,
        "open": 7931.71147491,
        "timestamp": 1558483200000,
        "value": 10794.67784198
      },
      {
        "close": 7741.034322,
        "high": 7864,
        "low": 7436.4,
        "open": 7620,
        "timestamp": 1558569600000,
        "value": 8793.37402799
      }
    ],
  bittrex: [
    {
      "close": 5403.984,
      "high": 5419.188,
      "low": 5310,
      "open": 5325.815,
      "timestamp": 1556755200000,
      "value": 398.70670515
    },
    {
      "close": 5699.683,
      "high": 5794.2,
      "low": 5367.949,
      "open": 5390,
      "timestamp": 1556841600000,
      "value": 1116.63944662
    },
    {
      "close": 5770.242,
      "high": 5831.288,
      "low": 5500,
      "open": 5669.995,
      "timestamp": 1556928000000,
      "value": 635.47650997
    },
    {
      "close": 5730.068,
      "high": 5780,
      "low": 5631.186,
      "open": 5770.241,
      "timestamp": 1557014400000,
      "value": 303.11500277
    },
    {
      "close": 5685.612,
      "high": 5751.72,
      "low": 5570.141,
      "open": 5710.981,
      "timestamp": 1557100800000,
      "value": 468.57474772
    },
    {
      "close": 5753.769,
      "high": 5963.382,
      "low": 5692.538,
      "open": 5692.538,
      "timestamp": 1557187200000,
      "value": 720.93875213
    },
    {
      "close": 5972.169,
      "high": 5983.6,
      "low": 5664.178,
      "open": 5750.327,
      "timestamp": 1557273600000,
      "value": 526.19133873
    },
    {
      "close": 6152.9,
      "high": 6174.646,
      "low": 5936.701,
      "open": 5940,
      "timestamp": 1557360000000,
      "value": 692.79352353
    },
    {
      "close": 6345,
      "high": 6424.49,
      "low": 6106.95,
      "open": 6152.9,
      "timestamp": 1557446400000,
      "value": 887.90044608
    },
    {
      "close": 7226.01,
      "high": 7449.453,
      "low": 6345,
      "open": 6345,
      "timestamp": 1557532800000,
      "value": 2334.10449786
    },
    {
      "close": 6972.99,
      "high": 7575,
      "low": 6763.702,
      "open": 7222.678,
      "timestamp": 1557619200000,
      "value": 2569.41248032
    },
    {
      "close": 7814.376,
      "high": 8157.002,
      "low": 6865.16,
      "open": 6980.397,
      "timestamp": 1557705600000,
      "value": 2835.47416176
    },
    {
      "close": 7989.85,
      "high": 8320,
      "low": 7616,
      "open": 7814.376,
      "timestamp": 1557792000000,
      "value": 1922.14604716
    },
    {
      "close": 8195.79,
      "high": 8297.647,
      "low": 7844.19,
      "open": 7989.85,
      "timestamp": 1557878400000,
      "value": 1344.00020733
    },
    {
      "close": 7889,
      "high": 8382.558,
      "low": 7654.09,
      "open": 8201.788,
      "timestamp": 1557964800000,
      "value": 1659.38877592
    },
    {
      "close": 7361.252,
      "high": 7936.1,
      "low": 6635.029,
      "open": 7877.612,
      "timestamp": 1558051200000,
      "value": 2742.3049817
    },
    {
      "close": 7271.825,
      "high": 7482.999,
      "low": 7220.429,
      "open": 7364.405,
      "timestamp": 1558137600000,
      "value": 705.22015805
    },
    {
      "close": 8185.427,
      "high": 8294.52,
      "low": 7262.186,
      "open": 7264.61,
      "timestamp": 1558224000000,
      "value": 1577.3966523
    },
    {
      "close": 8007.935,
      "high": 8200,
      "low": 7581.54,
      "open": 8195.484,
      "timestamp": 1558310400000,
      "value": 1413.23003081
    },
    {
      "close": 7950.89,
      "high": 8109.546,
      "low": 7812.21,
      "open": 8007.129,
      "timestamp": 1558396800000,
      "value": 824.66352521
    },
    {
      "close": 7632.74,
      "high": 8042,
      "low": 7510,
      "open": 7961.619,
      "timestamp": 1558483200000,
      "value": 823.57780161
    },
    {
      "close": 7751.727,
      "high": 7872.233,
      "low": 7478.064,
      "open": 7614.87,
      "timestamp": 1558569600000,
      "value": 632.13448156
    }
  ]
}

let res = []
for (const prop in data) {
  data[prop].map(e => {
    e.symbol = prop
    res.push(e)
  })
}
// console.log(JSON.stringify(res))

res = { "ohlcv": [{ "close": 5723.8, "high": 5784.3, "low": 5611.1, "open": 5629.6, "timestamp": 1556755200000, "value": 11075.8368328, symbol: "bitfinex BTC/USD" }, { "close": 6017, "high": 6150, "low": 5702.29694186, "open": 5736.6, "timestamp": 1556841600000, "value": 19480.28183518, "symbol": "bitfinex BTC/USD" }, { "close": 6098.1, "high": 6189.9903581, "low": 5863.4, "open": 6019.9, "timestamp": 1556928000000, "value": 9437.42259102, "symbol": "bitfinex BTC/USD" }, { "close": 6041.4021233, "high": 6112.3, "low": 5956, "open": 6098.1649927, "timestamp": 1557014400000, "value": 5815.53348677, "symbol": "bitfinex BTC/USD" }, { "close": 6027.9, "high": 6102, "low": 5901, "open": 6041.4021233, "timestamp": 1557100800000, "value": 6477.39950895, "symbol": "bitfinex BTC/USD" }, { "close": 6100, "high": 6325, "low": 6028.9, "open": 6028.9, "timestamp": 1557187200000, "value": 11990.22369015, "symbol": "bitfinex BTC/USD" }, { "close": 6201.5, "high": 6230, "low": 6007.8, "open": 6103.8, "timestamp": 1557273600000, "value": 8222.18279599, "symbol": "bitfinex BTC/USD" }, { "close": 6262, "high": 6344, "low": 6153.4575144, "open": 6201.8, "timestamp": 1557360000000, "value": 9682.27718866, "symbol": "bitfinex BTC/USD" }, { "close": 6438.7, "high": 6522.2, "low": 6212.2, "open": 6262, "timestamp": 1557446400000, "value": 14493.48955268, "symbol": "bitfinex BTC/USD" }, { "close": 7125.1, "high": 7279, "low": 6438.7, "open": 6438.7, "timestamp": 1557532800000, "value": 37945.37174068, "symbol": "bitfinex BTC/USD" }, { "close": 6983.4, "high": 7448, "low": 6858, "open": 7125.1, "timestamp": 1557619200000, "value": 31958.82528419, "symbol": "bitfinex BTC/USD" }, { "close": 7770.7, "high": 8050, "low": 6879.9649408, "open": 6980.2, "timestamp": 1557705600000, "value": 36467.14132655, "symbol": "bitfinex BTC/USD" }, { "close": 7951.9, "high": 8194.1, "low": 7651, "open": 7770.7, "timestamp": 1557792000000, "value": 29342.89533348, "symbol": "bitfinex BTC/USD" }, { "close": 8136.6341756, "high": 8209, "low": 7832.45175911, "open": 7951.9, "timestamp": 1557878400000, "value": 19036.43866675, "symbol": "bitfinex BTC/USD" }, { "close": 7859.7, "high": 8352.3, "low": 7700.1, "open": 8132.5, "timestamp": 1557964800000, "value": 22313.8762668, "symbol": "bitfinex BTC/USD" }, { "close": 7417.9, "high": 7910, "low": 6999.9105737, "open": 7860.4, "timestamp": 1558051200000, "value": 29021.92287556, "symbol": "bitfinex BTC/USD" }, { "close": 7260.1, "high": 7518, "low": 7207.6, "open": 7421.7, "timestamp": 1558137600000, "value": 9319.33096968, "symbol": "bitfinex BTC/USD" }, { "close": 8188.3, "high": 8269.2, "low": 7249.34672701, "open": 7260, "timestamp": 1558224000000, "value": 23458.30829652, "symbol": "bitfinex BTC/USD" }, { "close": 7990.6, "high": 8188, "low": 7600.1, "open": 8185.10102918, "timestamp": 1558310400000, "value": 16901.81605768, "symbol": "bitfinex BTC/USD" }, { "close": 7931.71147491, "high": 8094, "low": 7814.8, "open": 7985.1, "timestamp": 1558396800000, "value": 7692.99194632, "symbol": "bitfinex BTC/USD" }, { "close": 7620, "high": 8039.9, "low": 7500, "open": 7931.71147491, "timestamp": 1558483200000, "value": 10794.67784198, "symbol": "bitfinex BTC/USD" }, { "close": 7741.034322, "high": 7864, "low": 7436.4, "open": 7620, "timestamp": 1558569600000, "value": 8793.37402799, "symbol": "bitfinex BTC/USD" }, { "close": 5403.984, "high": 5419.188, "low": 5310, "open": 5325.815, "timestamp": 1556755200000, "value": 398.70670515, "symbol": "bittrex BTC/USD" }, { "close": 5699.683, "high": 5794.2, "low": 5367.949, "open": 5390, "timestamp": 1556841600000, "value": 1116.63944662, "symbol": "bittrex BTC/USD" }, { "close": 5770.242, "high": 5831.288, "low": 5500, "open": 5669.995, "timestamp": 1556928000000, "value": 635.47650997, "symbol": "bittrex BTC/USD" }, { "close": 5730.068, "high": 5780, "low": 5631.186, "open": 5770.241, "timestamp": 1557014400000, "value": 303.11500277, "symbol": "bittrex BTC/USD" }, { "close": 5685.612, "high": 5751.72, "low": 5570.141, "open": 5710.981, "timestamp": 1557100800000, "value": 468.57474772, "symbol": "bittrex BTC/USD" }, { "close": 5753.769, "high": 5963.382, "low": 5692.538, "open": 5692.538, "timestamp": 1557187200000, "value": 720.93875213, "symbol": "bittrex BTC/USD" }, { "close": 5972.169, "high": 5983.6, "low": 5664.178, "open": 5750.327, "timestamp": 1557273600000, "value": 526.19133873, "symbol": "bittrex BTC/USD" }, { "close": 6152.9, "high": 6174.646, "low": 5936.701, "open": 5940, "timestamp": 1557360000000, "value": 692.79352353, "symbol": "bittrex BTC/USD" }, { "close": 6345, "high": 6424.49, "low": 6106.95, "open": 6152.9, "timestamp": 1557446400000, "value": 887.90044608, "symbol": "bittrex BTC/USD" }, { "close": 7226.01, "high": 7449.453, "low": 6345, "open": 6345, "timestamp": 1557532800000, "value": 2334.10449786, "symbol": "bittrex BTC/USD" }, { "close": 6972.99, "high": 7575, "low": 6763.702, "open": 7222.678, "timestamp": 1557619200000, "value": 2569.41248032, "symbol": "bittrex BTC/USD" }, { "close": 7814.376, "high": 8157.002, "low": 6865.16, "open": 6980.397, "timestamp": 1557705600000, "value": 2835.47416176, "symbol": "bittrex BTC/USD" }, { "close": 7989.85, "high": 8320, "low": 7616, "open": 7814.376, "timestamp": 1557792000000, "value": 1922.14604716, "symbol": "bittrex BTC/USD" }, { "close": 8195.79, "high": 8297.647, "low": 7844.19, "open": 7989.85, "timestamp": 1557878400000, "value": 1344.00020733, "symbol": "bittrex BTC/USD" }, { "close": 7889, "high": 8382.558, "low": 7654.09, "open": 8201.788, "timestamp": 1557964800000, "value": 1659.38877592, "symbol": "bittrex BTC/USD" }, { "close": 7361.252, "high": 7936.1, "low": 6635.029, "open": 7877.612, "timestamp": 1558051200000, "value": 2742.3049817, "symbol": "bittrex BTC/USD" }, { "close": 7271.825, "high": 7482.999, "low": 7220.429, "open": 7364.405, "timestamp": 1558137600000, "value": 705.22015805, "symbol": "bittrex BTC/USD" }, { "close": 8185.427, "high": 8294.52, "low": 7262.186, "open": 7264.61, "timestamp": 1558224000000, "value": 1577.3966523, "symbol": "bittrex BTC/USD" }, { "close": 8007.935, "high": 8200, "low": 7581.54, "open": 8195.484, "timestamp": 1558310400000, "value": 1413.23003081, "symbol": "bittrex BTC/USD" }, { "close": 7950.89, "high": 8109.546, "low": 7812.21, "open": 8007.129, "timestamp": 1558396800000, "value": 824.66352521, "symbol": "bittrex BTC/USD" }, { "close": 7632.74, "high": 8042, "low": 7510, "open": 7961.619, "timestamp": 1558483200000, "value": 823.57780161, "symbol": "bittrex BTC/USD" }, { "close": 7751.727, "high": 7872.233, "low": 7478.064, "open": 7614.87, "timestamp": 1558569600000, "value": 632.13448156, "symbol": "bittrex BTC/USD" }] }
// return
chart.draw(res.ohlcv)
  .then(resp => { console.log(resp) })
  .catch(err => { console.log(err) })

// func.handle( res )
//   .then(resp => { console.log(resp) })
  // .catch(err => { console.log(err) })
