const margin = { top: 30, right: 20, bottom: 80, left: 50 },
  width = 900 - margin.left - margin.right,
  height = 400 - margin.top - margin.bottom

class Chart {

  constructor(d3, data, selection) {
    this.d3 = d3
    this.data = data
    this.elem = selection[0][0]
  }

  render() {

    const x = this.d3.time.scale().range([0, width])
    const y = this.d3.scale.linear().range([height, 0])

    const xAxis = this.d3.svg.axis().scale(x)
      .orient("bottom").ticks(this.d3.timeMinute, 5)

    const yAxis = this.d3.svg.axis().scale(y)
      .orient("left").ticks(10)

    const priceline = this.d3.svg.line()
      .x(d => x(d.date))
      .y(d => y(d.close))

    this.d3.select(this.elem).select('svg').remove()

    const svg = this.d3.select(this.elem)
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")")

    svg.append('rect')
      .attr('width', '110%')
      .attr('height', '100%')
      .attr('x', '-' + margin.left)
      .attr('y', '-' + margin.top)
      .attr('class', 'bg-rect')


    this.data.forEach(d => {
      d.date = (d.timestamp)
      d.close = +d.close
    })

    x.domain(this.d3.extent(this.data, d => d.date))
    y.domain([0, this.d3.max(this.data, d => d.close)])

    const dataNest = this.d3.nest()
      .key(d => d.symbol)
      .entries(this.data)

    dataNest.forEach((d, i) => {
      svg.append("path")
        .attr("class", "line" + i)
        .attr("d", priceline(d.values))
        .attr("data-legend", d.key)
        .attr('data-legend-pos', i)
    })

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)

    if (this.data[0].symbol)
      svg.append("g")
        .attr("class", "legend")
        .attr("transform", `translate(0,${height + 50})`)
        .style("font-size", "12px")
        .call(this.legend, this.d3, svg)

  }

  legend(g, d3, svg) {

    const fontSize = 14;
    const lRadius = 10;

    const getTextWidth = (text, fontSize) => {
      var canvas = document.createElement('canvas');
      var context = canvas.getContext('2d');
      context.font = fontSize + 'px ';
      return context.measureText(text).width;
    }
    g.each(function (e) {
      let g = d3.select(this),
        items = {},
        li = g.selectAll(".legend-items").data([true])

      // const rect = lb.enter().append("rect").classed("legend-box", true).attr('id', 'lrect')
      li.enter().append("g").classed("legend-items", true)


      svg.selectAll("[data-legend]").each(function () {
        const self = d3.select(this)
        items[self.attr("data-legend")] = {
          pos: self.attr("data-legend-pos") || this.getBBox().y,
          color: self.attr("data-legend-color") != undefined ? self.attr("data-legend-color") : self.style("fill") != 'none' ? self.style("fill") : self.style("stroke")
        }
      })

      items = d3.entries(items).sort((a, b) => a.value.pos - b.value.pos)

      const text = li.selectAll("text")
        .data(items, d => d.key)
        .call(d => d.enter().append("text"))
        .call(d => d.exit().remove())
        .attr("y", (d, i) => 0.3 + "em")
        .attr("x", (d, i) => {
          if (i > 0) {
            return getTextWidth(items[i - 1].key, fontSize) + (i * 2 * fontSize) + 35
          } else {
            return "2em"
          }
        })
        .attr('font-size', '14')
        .text(d => d.key)

      li.append("circle")
        .data(items, d => d.key)
        .call(d => d.enter().append("circle"))
        .call(d => d.exit().remove())
        .attr("cy", 0)
        .attr("cx", (d, i) => {
          if (i > 0) {
            return getTextWidth(items[i - 1].key, fontSize) + (i * 2 * fontSize) + 20
          } else {
            return "1em"
          }
        })
        .attr("r", lRadius)
        .attr('class', (d, i) => 'color' + i)

    })
    return g
  }


}

module.exports = Chart