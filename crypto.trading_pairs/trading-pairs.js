const a8 = require('@autom8/js-a8-fdk')
// const a8 = require('js-a8-link')('http://localhost:3035')

const chart = async (params) => {

  const ohlcv = []
  await Promise.all(
    params.pairs.map(async pair => {
      const data = await a8.ccxt.ohlcv({
        ...params,
        exchange: pair.exchange,
        currency: pair.currency
      })
      if (data.error) {
        throw new Error(data.error)
      }
      data.ohlcv.map(e => {
        ohlcv.push({ ...e, symbol: pair.exchange + ' ' + pair.currency })
      })
    })
  )

  let colors

  if (params.theme == 'dark')
    colors = {
      background: '#292f3b',
      xAxis: 'darkgrey',
      color1: 'orange',
      color2: 'steelblue',
    }
  else
    colors = {
      background: '#ffffff',
      xAxis: 'grey',
      color1: 'orange',
      color2: 'steelblue',
    }

  console.log(colors)
  const chart = await a8.graphs.multiline_chart({ ohlcv, colors })

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.data
}

module.exports = { chart }