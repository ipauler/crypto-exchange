const margin = {top: 30, right: 20, bottom: 80, left: 50},
  width = 900 - margin.left - margin.right,
  height = 400 - margin.top - margin.bottom

// const margin = { top: 20, right: 20, bottom: 30, left: 40 }

class Chart {

  constructor(d3, data, selection) {
    this.d3 = d3
    this.data = data
    this.elem = selection[0][0]
    // this.elem2 = selection[0][0]
  }

  render() {
    const margin = {top: 12, right: 10, bottom: 50, left: 20},
      width = 820 - margin.left - margin.right,
      height = 420 - margin.top - margin.bottom


    const svg = this.d3.select(this.elem).append('svg')
      .attr('width', 820 + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform',
        'translate(' + margin.left + ',' + margin.top + ')')

    svg.append('rect')
      .attr('width', '110%')
      .attr('height', '100%')
      .attr('x', '-' + margin.left)
      .attr('y', '-' + margin.top)
      .attr('class', 'bg-rect')

    this.draw(svg, 'bids', width / 2, height, margin)
    this.draw(svg, 'asks', width / 2, height, {...margin, left: width / 2 + margin.left})

  }


  draw(svg, type, width, height, margin) {
    console.log(margin)
    const g = svg.append('g')
      .attr('transform', `translate(${10},${margin.top})`)

    const datab = this.data[type]

    const x = this.d3.scale.ordinal()
      .rangeRoundBands([0, width])

    const y = this.d3.scale.linear()
      .range([height, 0])

    x.domain([
      this.d3.min(datab, d => d[1]),
      this.d3.max(datab, d => d[1]) + 1
    ])
    y.domain([0, this.d3.max(datab, d => d[1])])

    const xAxis = this.d3.svg.axis().scale(x)
      .orient('bottom')
      .ticks(15)
      .tickSize(1)
    // .ticks(this.d3.timeMinute, 5)

    const yAxis = this.d3.svg.axis().scale(y)
      .orient('left')
      .tickSize(0)
    // .ticks(10)

    g.append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(${margin.left - 20},${height + 2})`)
      .call(xAxis)


    if (type == 'bids')
      g.append('g')
        .attr('class', 'y-axis')
        .call(yAxis)

    let data = []
    const __cum_data = []
    for (let i = 0; i < this.data[type].length; i++) {
      __cum_data.push(this.data[type][i][1])
    }
    const cum_data_array = this.prefixSum(__cum_data)

    for (let i = 0; i < this.data[type].length; i++) {
      data.push({
        idx: this.data[type][i][0],
        orders: cum_data_array[i]
      })
    }

    if (type == 'bids')
      data = data.reverse()

    data.forEach(function (d) {
      d.orders = +d.orders
    })

    x.domain(data.map(d => d.idx))
    y.domain([0, this.d3.max(data, d => d.orders)])

    svg.selectAll('.bar-' + type)
      .remove('rect')

    svg.selectAll('.bar')
      .data(data)
      .enter().append('rect')
      .attr('class', type)
      .attr('x', d => x(d.idx))
      .attr('width', x.rangeBand() - 1)
      .attr('y', d => y(d.orders))
      .attr('height', d => height - y(d.orders))
      .attr('transform', `translate( ${margin.left - 18} , ${margin.top} )`)

  }

  prefixSum(arr) {
    return arr.reduce((acc, n) => {
      const lastNum = acc.length > 0 ? acc[acc.length - 1] : 0
      acc.push(lastNum + n)
      return acc
    }, [])
  }

}

module.exports = Chart