const fdk = require('@autom8/fdk');
const a8 = require('@autom8/js-a8-fdk');
const priceGraph = require('./price-graph');

const handle = (input) => {
	
	return priceGraph.graph(input)
		.then(response => {
			return { "data": response }
		})
		.catch(err => {
			return { "error": err }
		})
}

const slack = (result) => {
	let responseType = "in_channel";
	const blocks = [];

	if (!result.error) {
		if (result.data.length > slackLimit)
			result.data = result.data.slice(result.data.length - slackLimit, result.data.length);

		blocks.push({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": " ``` " + JSON.stringify(result.data) + " ``` "
			}
		});


	} else {
		responseType = "ephemeral"
		blocks.push({
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": "Error generating chart."
			}
		});
	}

	return {
		"response_type": responseType,
		"blocks": blocks
	}
}

const discord = (result) => {

	if (!result.error) {
		if (result.data.length > discordLimit)
			result.data = result.data.slice(result.data.length - discordLimit, result.data.length);
		return {
			"content": "``` " + JSON.stringify(result.data) + " ```"
		}
	}
	else {
		return {
			"content": "Error generating chart."
		}
	}
}

fdk.handle(handle);
fdk.slack(slack);
fdk.discord(discord);
