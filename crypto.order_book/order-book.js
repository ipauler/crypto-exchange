const a8 = require('@autom8/js-a8-fdk')
// const a8 = require('js-a8-link')('http://localhost:3035')

const chart = async (params) => {

  const data = await a8.ccxt.order_book(params)
  if (data.error) {
    throw new Error(data.error)
  }
  console.log(JSON.stringify(data))
  const chart = await a8.graphs.order_book_chart(data)

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.data
}

module.exports = {chart}