const fdk = require('@autom8/fdk')
const orderBook = require('./order-book')

const handle = async (input) => {
  const params = {
    currency: input.currency,
    exchange: input.exchange
  }


  if (!params.currency)
    return {error: 'Please enter currency'}
  else if (!input.exchange)
    return {error: 'Please enter exchange'}
  else {
    try {
      const response = await orderBook.fetch(params)
      return {data: response}
    } catch (err) {
      return {error: err.message}
    }
  }
}

const slack = (result) => {
  let responseType = 'in_channel'
  const blocks = []

  if (!result.error) {

    blocks.push({
      'type': 'section',
      'text': {
        'type': 'mrkdwn',
        'text': ' ``` ' + JSON.stringify(result) + ' ``` '
      }
    })


  } else {
    responseType = 'ephemeral'
    blocks.push({
      'type': 'section',
      'text': {
        'type': 'mrkdwn',
        'text': 'Error retrieving ohlcv data.'
      }
    })
  }

  return {
    'response_type': responseType,
    'blocks': blocks
  }
}
const discord = (result) => {

  if (!result.error) {
    return {
      'content': '``` ' + JSON.stringify(result) + ' ```'
    }
  } else {
    return {
      'content': 'Error generating chart.'
    }
  }
}
// module.exports = { handle }
fdk.handle(handle)
fdk.slack(slack)
fdk.discord(discord)