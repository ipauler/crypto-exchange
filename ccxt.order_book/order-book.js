const ccxt = require('ccxt')
const moment = require('moment')
moment.suppressDeprecationWarnings = true

const fetch = async (params) => {

  if (ccxt.exchanges.indexOf(params.exchange) === -1)
    throw new Error('Sorry this exchange market is not supported')

  const { bids, asks } = await new ccxt[params.exchange]().fetchOrderBook(params.currency)

  return { bids, asks }
}

module.exports = { fetch }